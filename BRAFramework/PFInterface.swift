//
//  PFInterface.swift
//  BRAFramework
//
//  Created by Timothy Barrett on 7/30/16.
//  Copyright © 2016 BRA LLC. All rights reserved.
//

import UIKit
import Parse
public class PFInterface : NSObject {
    
    
    public struct ErrorMessages {
        static let NoObjectID:String = "The object does not have an ID"
        static let InvalidObjectFromServer:String = "The server did not return the correct object"
    }
    
    private override init() {
        super.init()
        
    }
    
    public static func getAllRequests(completion:(requests:[Request], error:NSError?)->Void) {
        PFCloud.callFunctionInBackground("getAllRequests", withParameters: nil) { (objects, error) in
            if error == nil {
                if let objects = objects as? NSArray {
                    var requests = [Request]()
                    for requestObject in objects {
                        if let request = requestObject as? Request {
                            requests.append(request)
                        }
                    }
                    completion(requests: requests, error: nil)
                } else {
                    completion(requests: [Request](), error: NSError(domain: "PFInterface", code: 0, userInfo: [NSLocalizedDescriptionKey:"Could not parse returned results"]))
                }
            } else {
                completion(requests: [Request](), error: error)
            }
        }
    }
    
    public static func getAllProducts(completion:(products:[Product], error:NSError?)->Void) {
        PFCloud.callFunctionInBackground("getAllProducts", withParameters: nil) { (products, error) in
            if error == nil {
                guard let products = products as? NSArray else {
                    completion(products: [Product](), error: NSError(domain: "PFInterface", code: 0, userInfo: [NSLocalizedDescriptionKey:"Could not parse returned results"]))
                    return
                }
                var prods = [Product]()
                for item in products {
                    print(item)
                    if let productObject = item as? Product {
                        prods.append(productObject)
                    }
                }
                completion(products: prods, error: nil)
            } else {
                completion(products: [Product](), error: error)
            }
        }
    }
    
    // MARK: PFUser Management
    
    public static func authenticate(withUsername username:String, withPassword password:String, completion:(user:PFUser?, error:NSError?)->Void) {
        PFUser.logInWithUsernameInBackground(username, password: password) { (user, error) in
            completion(user: user, error: error)
        }
    }
    
    public static func signUp(withUsername username:String, withPassword password:String, withEmail email:String, withPhone phone:String, completion:(success:Bool, error:NSError?)->Void) {
        let user = PFUser()
        user.username = username
        user.password = password
        user.email = email
        user["phone"] = phone
        user.signUpInBackgroundWithBlock { (success, error) in
            completion(success: success, error: error)
        }
    }
    
    // TODO: Fill in adding payment method
    /* Use method to add a payment method and also update a payment method */
    public static func addPaymentMethod(userID:String, cardNumber:String, cvc:String, expMonth:Int, expYear:Int, completion:(error:NSError?)->Void) {
        // card number
        // cvc
        // exp year
        // exp month
        // user object ID
        
        // server returns wrapped json object customer and pfuser
        /*
             {
             "objectId":"mD913Gx2jU",
             "exp_month":"10",
             "exp_year":"2020",
             "number":"4242424242424242",
             "cvc":"345"
             }
        */
        PFCloud.callFunctionInBackground("createCustomer", withParameters: ["objectId":userID, "number":cardNumber, "cvc":cvc, "exp_month":expMonth, "exp_year":expYear]) { (object, error) in
            if error == nil {
                if object != nil {
                    completion(error: nil)
                } else {
                    completion(error: error)
                }
            } else {
                completion(error: error)
            }
            
        }
 
    }
    
    // MARK: Request Handeling
    public static func pickupRequest(captainUserId:String, request:Request, completion:(request:Request?, error:NSError?)->Void) {
        if request.objectId != nil {
            PFCloud.callFunctionInBackground("pickupRequest", withParameters: ["requestId": request.objectId!, "userId": captainUserId], block: { (requestObject, error) in
                if error == nil {
                    if let request = requestObject as? Request {
                        completion(request: request, error: nil)
                    } else {
                        completion(request: nil, error: PFInterface.createError(withMessage: ErrorMessages.InvalidObjectFromServer))
                    }
                } else {
                    completion(request: nil, error: error)
                }
            })
        } else {
            completion(request: nil, error: PFInterface.createError(withMessage: ErrorMessages.NoObjectID))
        }
    }
    
    /*
     {
     "requestId":"6qSThQDEQ1",
     "userId":"4GU3H3RBgi",
     "captainId":"4GU3H3RBgi",
     "productId":"YIQesj26aL"
     }
    */
 
    public static func dropoffRequest(captainUserId:String, passengerUserId:String, request:Request, completion:(request:Request?, error:NSError?)->Void) {
        if request.objectId != nil && request.product?.objectId != nil {
            PFCloud.callFunctionInBackground("createCharge", withParameters: ["requestId":request.objectId!, "userId":passengerUserId, "captainId":captainUserId, "productId":request.product!.objectId!], block: { (object, error) in
                if error == nil {
                    print(object)
                    if let completedRequest = object as? Request {
                        completion(request: completedRequest, error: nil)
                    } else {
                        completion(request: nil, error: PFInterface.createError(withMessage: ErrorMessages.InvalidObjectFromServer))
                    }
                } else {
                    completion(request: nil, error: error)
                }
            })
        } else {
            completion(request: nil, error: PFInterface.createError(withMessage: ErrorMessages.NoObjectID))
        }
    }
    
    private static func createError(withMessage message:String) -> NSError {
        return NSError(domain: "PFInterface", code: 0, userInfo: [NSLocalizedDescriptionKey:message])
    }
}

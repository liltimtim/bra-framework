//
//  LocationTrackingManager.swift
//  BRAFramework
//
//  Created by Timothy Barrett on 7/31/16.
//  Copyright © 2016 BRA LLC. All rights reserved.
//
//  Class should be used to watch for a user's location not an individual location
import Foundation
import Parse
@objc public protocol LocationTrackingManagerDelegate : class {
    optional func userLocationUpdated(toNewLocation:LocationTracking?)
    optional func userLocationThrewError(error:NSError?)
}
public class LocationTrackingManager : NSObject {
    public weak var delegate:LocationTrackingManagerDelegate?
    private var refreshDataTimer:NSTimer?
    private var trackedUser:PFUser!
    private override init() { super.init() }
    
    public init(begingTrackingUser user:PFUser) {
        super.init()
        self.trackedUser = user
        self.refreshDataTimer = NSTimer.init(timeInterval: 1.0, target: self, selector: #selector(refreshData), userInfo: nil, repeats: true)
        NSRunLoop.currentRunLoop().addTimer(self.refreshDataTimer!, forMode: NSRunLoopCommonModes)
    }
    
    deinit {
        self.refreshDataTimer?.invalidate()
        self.refreshDataTimer = nil
    }
    
    internal func refreshData() {
        if self.trackedUser.objectId != nil {
            let query = PFQuery(className: LocationTracking.parseClassName())
            query.orderByDescending("createdAt")
            query.whereKey("user", equalTo: self.trackedUser)
            query.getFirstObjectInBackgroundWithBlock({ (object, error) in
                if error == nil {
                    if let location = object as? LocationTracking {
                        self.delegate?.userLocationUpdated?(location)
                    } else {
                        self.delegate?.userLocationThrewError?(LocationTrackingManager.createError(withMessage: "Server returned invalid object"))
                    }
                } else {
                    self.delegate?.userLocationThrewError?(error)
                }
            })
        } else {
            self.refreshDataTimer?.invalidate()
            self.refreshDataTimer = nil
            self.delegate?.userLocationThrewError?(LocationTrackingManager.createError(withMessage: "the requested user to track does not have an object ID"))
        }

    }
    
    private static func createError(withMessage message:String) -> NSError {
        return NSError(domain: "LocationTrackingManager", code: 0, userInfo: [NSLocalizedDescriptionKey: message])
    }
    
}

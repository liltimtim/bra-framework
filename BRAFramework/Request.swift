//
//  File.swift
//  BRAFramework
//
//  Created by Timothy Barrett on 7/30/16.
//  Copyright © 2016 BRA LLC. All rights reserved.
//

import Foundation
import Parse
import ParseLiveQuery
@objc public protocol RequestDelegate: class {
    optional func requestUpdated(request:Request)
    optional func requestError(error:NSError?)
}
public class Request : PFObject, PFSubclassing {
    
    private var refreshTimer:NSTimer?
    weak var delegate:RequestDelegate?
    @NSManaged public var requester:PFUser?
    @NSManaged public var pickupPoint:PFGeoPoint?
    @NSManaged public var pickupTime:NSDate?
    @NSManaged public var dropoffTime:NSDate?
    @NSManaged public var captain:PFUser?
    @NSManaged public var product:Product?
    @NSManaged public var charge:Charge?
    public var chargeAmount:Double? {
        set { if newValue != nil { self["chargeAmount"] = newValue! } }
        get { return self["chargeAmount"] as? Double }
    }
    public var chargeCompleted:Bool? {
        set { if newValue != nil { self["chargeCompleted"] = newValue! }}
        get { return self["chargeCompleted"] as? Bool }
    }
    public var cancelled:Bool? {
        set { if newValue != nil { self["cancelled"] = newValue! }}
        get { return self["cancelled"] as? Bool }
    }
    public static func parseClassName() -> String {
        return "Request"
    }
    
    deinit {
        
    }
}

extension Request {
    func subscribe() {
        self.refreshTimer = NSTimer.init(timeInterval: 5, target: self, selector: #selector(beginUpdatingRequest), userInfo: nil, repeats: true)
        NSRunLoop.currentRunLoop().addTimer(self.refreshTimer!, forMode: NSRunLoopCommonModes)
    }
    
    func unsubscribe() {
        self.refreshTimer?.invalidate()
        self.refreshTimer = nil
    }
    
    internal func beginUpdatingRequest() {
        self.fetchInBackgroundWithBlock { (object, error) in
            if error == nil {
                self.delegate?.requestUpdated?(self)
            } else {
                self.delegate?.requestError?(error)
            }
        }
    }
}
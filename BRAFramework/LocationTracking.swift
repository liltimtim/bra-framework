//
//  Tracking.swift
//  BRAFramework
//
//  Created by Timothy Barrett on 7/31/16.
//  Copyright © 2016 BRA LLC. All rights reserved.
//

import Foundation
import Parse
public class LocationTracking : PFObject, PFSubclassing {
    @NSManaged var user:PFUser?
    @NSManaged var location:PFGeoPoint?
    
    public static func parseClassName() -> String {
        return "LocationTracking"
    }
}
//
//  Product.swift
//  BRAFramework
//
//  Created by Timothy Barrett on 7/30/16.
//  Copyright © 2016 BRA LLC. All rights reserved.
//

import Foundation
import Parse
public class Product : PFObject, PFSubclassing {
    @NSManaged public var title: String?
    public var price: Double?
    public static func parseClassName() -> String {
        return "Products"
    }
}
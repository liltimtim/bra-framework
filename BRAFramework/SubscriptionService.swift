//
//  SubscriptionService.swift
//  BRAFramework
//
//  Created by Timothy Barrett on 7/31/16.
//  Copyright © 2016 BRA LLC. All rights reserved.
//

import Foundation
import Parse
import ParseLiveQuery
@objc public protocol SubscriptionServiceDelegate: class {
    optional func requestUpdated(requests:[Request])
    optional func requestEntered(request:Request?)
    optional func requestExited(request:Request?)
    optional func requestCreated(request:Request?)
    optional func requestError(error:NSError?)
}
public class SubscriptionService : NSObject {
    private var refreshTimer:NSTimer?
    weak public var delegate:SubscriptionServiceDelegate?
    
    public override init() {
        super.init()
        
    }
    
    deinit {
        self.refreshTimer?.invalidate()
        self.refreshTimer = nil
    }
    // TODO: Waiting on Bug Issue #16 to get fixed (GitHub)
    /**
     Bug in Parse Live Query where you cannot query against PFGeoPoints :( for now we will use simple logic and get all requests.
     
     Currently this will fire every 30 seconds
     
     - parameter point: PFGeoPoint
    */
    public func beginUpdatingRequests() {
        self.refreshTimer = NSTimer.init(timeInterval: 5, target: self, selector: #selector(refreshAvailableRequests), userInfo: nil, repeats: true)
        NSRunLoop.currentRunLoop().addTimer(self.refreshTimer!, forMode: NSRunLoopCommonModes)
    }
    
    public func stopUpdatingRequests() {
        self.refreshTimer?.invalidate()
        self.refreshTimer = nil
    }
    
    internal func refreshAvailableRequests() {
        let query = PFQuery(className: Request.parseClassName())
        query.whereKeyDoesNotExist("captain")
        query.whereKey("cancelled", equalTo: false)
        query.whereKeyDoesNotExist("cancelled")
        query.findObjectsInBackgroundWithBlock { (objects, error) in
            if error == nil {
                var requests = [Request]()
                if objects != nil {
                    for requestObject in objects! {
                        if let request = requestObject as? Request {
                            requests.append(request)
                        }
                    }
                    self.delegate?.requestUpdated?(requests)
                } else {
                    self.delegate?.requestError?(self.createError(withMessage: "The server did not return valid objects"))
                }
                
            } else {
                self.delegate?.requestError?(error)
            }
        }
    }
    
    private func createError(withMessage message:String) -> NSError {
        return NSError(domain: "SubscriptionService", code: 0, userInfo: [NSLocalizedDescriptionKey:message])
    }
}